package read

import (
	"bufio"
	"fmt"
	"net/url"
	"os"
)

func GetUrls() ([]string, error) {
	urls, err := readUrls()
	if err != nil {
		e := fmt.Errorf("reading input: %s", err)
		return nil, e
	}
	if err := validateUrls(urls); err != nil {
		e := fmt.Errorf("reading input: %s", err)
		return nil, e
	}
	return urls, nil
}

func readUrls() ([]string, error) {
	urls := []string{}
	scanner := bufio.NewScanner(os.Stdin)

	fmt.Println("Enter urls:")
	for scanner.Scan() {
		url := scanner.Text()
		urls = append(urls, url)
	}
	if err := scanner.Err(); err != nil {
		return nil, fmt.Errorf("reading input: %s", err)
	}

	return urls, nil
}

func validateUrls(urls []string) error {
	for _, u := range urls {
		if _, err := url.ParseRequestURI(u); err != nil {
			return err
		}
	}
	return nil
}
