# Kazdream Go Cli Task

## Setup

### Run

```sh
go run .
```

### Build

```sh
go build
```

## Usage

Type the urls required, after "Enter urls:". One for each. To execute command add End-of-Transmission or `Ctrl+D`.

You will see:
* "DATA" figures in csv format: `url;response code;document size;response delay`;
* "STATS" should be statistics of parallel runs. But couldn't figure out how to get thread identifier

Regardless of whether you'll wait the response or stop `Ctrl+C`, always will see "STATS".

**Record**

![](https://i.imgur.com/a5CVu4I.mp4)
