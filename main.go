package main

import (
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"gitlab.com/talgat.s/kazdream-task/read"
)

type data struct {
	url    string
	status int
	size   int
	delay  int64
	err    error
	valid  bool
}

func main() {
	runtime.GOMAXPROCS(0)
	// gracefull shutdown
	done := make(chan os.Signal, 1)
	sCh := make(chan map[int]int)
	// to ensure that request started
	startFlag := false
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func(done chan<- os.Signal, sCh chan<- map[int]int, startFlag *bool) {
		urls, err := read.GetUrls()
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			close(done)
			return
		}

		dCh := make(chan *data)
		pCh := make(chan int)

		*startFlag = true
		go request(pCh, dCh, urls)
		go collectStats(sCh, pCh, len(urls))
		logData(dCh, len(urls))

		close(done)
	}(done, sCh, &startFlag)

	<-done
	if !startFlag {
		close(sCh)
	} else {
		printStats(sCh)
	}
}

func request(pCh chan<- int, dCh chan<- *data, urls []string) {
	for _, u := range urls {
		go sendGetRequest(pCh, dCh, u)
	}
}

func sendGetRequest(pCh chan<- int, dCh chan<- *data, u string) {
	pCh <- os.Getpid() // Send the parallel identifier

	d := &data{}
	d.url = u

	start := time.Now()
	resp, err := http.Get(u)
	duration := time.Since(start)
	d.delay = duration.Milliseconds()
	if err != nil {
		fmt.Fprintf(os.Stderr, "request \"%s\" error: %s\n", u, err)
		dCh <- d
		return
	}

	d.status = resp.StatusCode

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Fprintf(os.Stderr, "request body \"%s\" error: %s\n", u, err)
	}
	d.size = len(body)
	d.valid = true

	dCh <- d
}

func collectStats(sCh chan<- map[int]int, pCh <-chan int, l int) {
	stats := make(map[int]int)
	for i := 0; i < l; i++ {
		p := <-pCh
		stats[p] += 1
	}

	sCh <- stats
}

func printStats(sCh <-chan map[int]int) {
	stats := <-sCh

	fmt.Println("\nSTATS")
	for id, num := range stats {
		fmt.Printf("%d:%d\n", id, num)
	}
}

func logData(dCh <-chan *data, l int) {
	csvs := collectData(dCh, l)
	printData(csvs)
}

func collectData(dCh <-chan *data, l int) [][]string {
	csvs := [][]string{}

	for i := 0; i < l; i++ {
		d := <-dCh
		if d.valid {
			s := []string{
				d.url,
				strconv.Itoa(d.status),
				strconv.Itoa(d.size),
				strconv.Itoa(int(d.delay)),
			}
			csvs = append(csvs, s)
		}
	}

	return csvs
}

func printData(csvs [][]string) {
	w := csv.NewWriter(os.Stdout)
	w.Comma = ';'

	for _, s := range csvs {
		if err := w.Write(s); err != nil {
			fmt.Fprintf(os.Stderr, "error writing record to csv:\n", err)
		}
	}

	fmt.Println("\nDATA")
	w.Flush()
	if err := w.Error(); err != nil {
		fmt.Fprintln(os.Stderr, "scv formating error:", err)
	}
}
